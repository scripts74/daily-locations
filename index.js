require('dotenv').config() // read database_url from .env file

const { PrismaClient } = require('./prisma/generated/client')
const prisma = new PrismaClient()

const fsPromisified = require('fs').promises;
const fs = require('fs')
const createCsvStringifier = require('csv-writer').createObjectCsvStringifier;

const delay = require('delay')
const axios = require('axios');
const dayjs = require('dayjs')
let httpsProxyAgent = require('https-proxy-agent');

var agent = new httpsProxyAgent(process.env.PROXY_AGENT);

const notifier = require('node-notifier');
const random_useragent = require('random-useragent');
var _ = require('lodash');
const schedule = require('node-schedule');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STATE VARIABLES FOR CSV SPLITTING 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Maximum size limit for each CSV file in bytes (5 MB default size)
const maxFileSize = (process.env.MAX_FILE_SIZE_MB || 5) * 1024 * 1024;

// State variables for splitDataIntoCSVFiles function
let currentFileIndex = 1

// 2 min default checking interval
const checkingIntervalMins = process.env.CHECK_FILE_SIZE_EVERY_X_MINS || 2

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let proxy = []
let sessions = []
var accounts = []
const batchSize = 5000
const batchDelay = 120
const intDelay = process.env.DELAY || 1000
const dayLimit = 16
let proxyCounter = 0
let sessionCounter = -1
let accountCounter = -1
let isWork = true
let isGo = true
let isChange = false
let myInterval = 0
let totalDone = 0
const inputTable = '"user"'
const outputTable = '"user"'
const sessionTable = 'sessions'
const locTable = 'location'
const postTable = 'posts'
let noResp = 0
let isDebug = true
let req = 0
let setAllUsers;
let logUrl = process.env.LOG_URL || 'https://ntfy.useapp.in/daily-locations'
let logUrlStat = process.env.LOG_URL || 'https://ntfy.useapp.in/daily-locations-stat'


const Start = async () => {

  // await handleDisconnect();
  // await getSessions()
  console.log('Starting main loop.')
  while (isWork) {
    if (accountCounter < 0) {

      console.log('Starting new batch.' + dayjs().format('HH:mm:ss'))
      totalDone = 0
      // await changeIP()
      // await getUsers()
      await delay(5000)
      await getIP()
      await getAccounts(batchSize)
      if (accounts.length > 0) {

        // myInterval = setInterval(getBio, intDelay)
        myInterval = setInterval(getBio, intDelay)

        // await delay (batchSize*intDelay)
        await delay(accounts.length * intDelay + 10000)
        // await (200000)

        // client.query(`INSERT INTO log (name,done,comment) VALUES ('bio','${totalDone}','rogproxy/d=500/b=200')`, function (error, results, fields) {
        //   if (error) throw error; 

        //   });

      } else {
        console.log('No more accounts. Wait 1 min. Time: ' + dayjs().format('HH:mm:ss'))
        await delay(60000)
      }
    } else {
      // console.log('Account counter >= 0. Wait 1 min. Time: ',accountCounter,dayjs().format('HH:mm:ss'))
      await delay(5000)
    }

  }


}

const getUsers = async () => {


  let allUsers = []

  try {

    allUsers = await prisma.user.findMany({
      where: {
        userId: {
          gt: 0
        }
      },
      select: {
        userId: true
      }
    });

  } catch (error) {
    console.log(error.message)
  }

  const valuesArray = allUsers.map(obj => Number(obj.userId));
  setAllUsers = new Set(valuesArray);

  console.log('Got users')
  console.log(setAllUsers.size)
  // console.log(setAllUsers)


}



function filterObjects(smallArray) {
  // Convert the large array to a Set for faster lookups

  // Use lodash's filter method to filter the small array
  const filteredObjects = _.filter(smallArray, (obj) => {
    // Assuming the field you're checking is named 'field'
    return setAllUsers.has(Number(obj.user_id));
  });

  return filteredObjects;
}





const getAccounts = async (size) => {


  accounts = []
  console.log('Get accounts func', dayjs().format('HH:mm:ss'))
  try {

    accounts = await prisma.location.findMany({
      where: {
        // countryId: 'ua',
        countryId: {
          in: ['ua', 'by', 'ru']
        },
        nul: 0,
        next: {
          lt: new Date(Date.now())
        }
        // delay: 0
        // isProcessed: 0
      },
      orderBy: {
        times: 'desc'
      },
      select: {
        locationId: true,
        name: true,
        delay: true,
        next: true,
        countryId: true
      },
      take: size
      // skip: 20000
    });

    if (accounts) {
      console.log('Accounts received. Time: ' + dayjs().format('HH:mm:ss'))
    }

  } catch (error) {
    console.log(error.message)
  }



}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CODE TO OUTPUT CSV FILE HERE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const csvStringifier = createCsvStringifier({
  header: [
    { id: 'user_id', title: 'USER_ID' },
    { id: 'full_name', title: 'FULL_NAME' },
    { id: 'is_private', title: 'IS_PRIVATE' },
    { id: 'username', title: 'USERNAME' },
    { id: 'cid', title: 'CID' }
  ]
});

async function saveToCSV(data, filename) {
  // Ensure data is an array of objects
  if (!Array.isArray(data) || !data.every(item => typeof item === 'object')) {
    throw new Error('Data must be an array of objects');
  }

  let csvContent
  try {
    if (fs.existsSync(filename)) {
      csvContent = csvStringifier.stringifyRecords(data)
    } else {
      csvContent = csvStringifier.getHeaderString() + csvStringifier.stringifyRecords(data)
    }
    await fsPromisified.appendFile(filename, csvContent)
  } catch (err) {
    console.log(err)
    console.log('error writing to file')
    throw err
  }
}


// // Function to write data to CSV file asynchronously
// const writeDataToCSV = async(filename, data) => {
//   csvHeaders = 'user_id,full_name,is_private,username,cyrl_loc\n'
//   csvRows = data.map(formatAsCSVRow)
//   csvContent = fs.existsSync(filename) ? csvRows.join('') : (csvHeaders + csvRows)

//   try {
//     await fsPromisified.appendFile(filename, csvContent, { encoding: 'utf8' });
//   } catch (err) {
//     console.log(err)
//     console.log('error writing to csv file')
//     throw err
//   }
// };

// check filesize every specified interval in mins
schedule.scheduleJob(`*/${checkingIntervalMins} * * * *`, async () => {
  filename = `./output/output_${currentFileIndex}.csv`
  if (fs.existsSync(filename)) {
    // check if current csv file we are writing to is greater than max file size
    if ((await fsPromisified.stat(filename)).size > maxFileSize) {
      currentFileIndex += 1 // we will write write to a new file from now on
      console.log(`We will be writing data to output_${currentFileIndex}.csv from now on!`)
    }
  }

})

// Function to split data into multiple CSV files asynchronously
const splitDataIntoCSVFiles = async (data) => {
  filename = `./output/output_${currentFileIndex}.csv`
  // await writeDataToCSV(filename, data) // write data to csv file
  await saveToCSV(data, filename) // write data to csv file

};

// will get the index of file that we should be writing to first if the script is restarted
const getCurrentFileIndex = (maxFileSize) => {
  // give maxFileSize for csv as an argument
  const files = fs.readdirSync('./output').filter(value => value.includes('output_'))

  var currentFileIndex = files.length

  if (fs.statSync(`./output/output_${currentFileIndex}.csv`).size >= maxFileSize) currentFileIndex += 1

  return currentFileIndex

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const getBio = async () => {

  // if (req % 100 == 0) {
  //   changeIP()
  // }

  if (accountCounter == batchSize - 1 || accountCounter == accounts.length - 1 || accounts.length == 0) {
    accountCounter = -1;
    clearInterval(myInterval)
    console.log('Batch finished' + dayjs().format('HH:mm:ss'))
    return;
  } else {
    accountCounter++
  }


  let locAccountCounter = accountCounter;

  console.log(`Starting location ${accounts[locAccountCounter].name}, ${accounts[locAccountCounter].locationId}. Time: ${dayjs().format('HH:mm:ss')}`)

  let locid = accounts[locAccountCounter].locationId;
  let cid = accounts[locAccountCounter].countryId;
  let nme = accounts[locAccountCounter].name;
  let dl = accounts[locAccountCounter].delay;


  let ua = random_useragent.getRandom();

  req++

  // console.time(locid);
  // console.timeLog(locid,'Axios start');

  axios.get(`https://www.instagram.com/api/v1/locations/web_info/?location_id=${accounts[locAccountCounter].locationId}`, {

    // axios.get(`https://www.instagram.com/api/v1/locations/web_info/?location_id=107677462599905`, {
    headers: {
      // 'User-Agent': randomUseragent.getRandom(),
      'User-Agent': ua,
      // 'Cookie': sessions[locSessionCounter].sessionid,
      'x-ig-app-id': '936619743392459'
    },
    httpsAgent: agent,
    timeout: 30000,
    withCredentials: true
  })
    .then(async (res) => {

      // console.log(locid,'Here');

      let lat;
      let lng;

      let newAccs = []
      let newAccs2 = []
      let timeRange = []
      let orNewAccs = []
      let orNewAccs2 = []
      let orTimeRange = []
      let delCal = 0
      let del = 0
      let time1
      let time2
      let nextTime
      let isRanked = false;

      if (res.data.native_location_data.ranked.sections.length > 0) {

        res.data.native_location_data.ranked.sections.map(async (item) => {

          if (item.layout_content.medias) {

            if (item.layout_content.medias.length > 0 && item.layout_type == 'media_grid') {

              item.layout_content.medias.map(async (item2) => {

                // setAllUsers.add(Number(item2.media.user.id))
                isRanked = true;
                newAccs.push({
                  user_id: item2.media.user.id,
                  full_name: item2.media.user.full_name ? item2.media.user.full_name.replace(/\n/g, " ").replace(/[^0-9a-zA-Z, а-яА-Я]/gi, '') : '',
                  is_private: item2.media.user.is_private ? 1 : 0,
                  username: item2.media.user.username,
                  cid: cid

                })

                // timeRange.push(item2.media.taken_at)

                if (item2.media.likers) {

                  if (item2.media.likers.length > 0) {

                    item2.media.likers.map(async (item3) => {

                      // setAllUsers.add(Number(item3.pk_id))

                      newAccs.push({
                        user_id: item3.pk_id,
                        full_name: item3.full_name ? item3.full_name.replace(/\n/g, " ").replace(/[^0-9a-zA-Z, а-яА-Я]/gi, '') : '',
                        is_private: item3.is_private ? 1 : 0,
                        username: item3.username,
                        cid: cid

                      })

                    })

                  }

                }

              })

            }

          }



        })


        // orTimeRange = _.orderBy(timeRange, item => item, ['desc'])
        orNewAccs = _.filter(newAccs, function (o) { return o.user_id < 9953125287 })
        orNewAccs = _.uniqBy(orNewAccs, 'user_id');


        if (orNewAccs.length > 0) {

          // saveToCSV(orNewAccs, 'data3.csv')
          try {
            splitDataIntoCSVFiles(orNewAccs)
          } catch (err) {
            console.error('error')
          }



        }



      }



      if (res.data.native_location_data.recent.sections.length > 0) {

        res.data.native_location_data.recent.sections.map(async (item) => {

          if (item.layout_content.medias.length > 0) {

            item.layout_content.medias.map(async (item2) => {

              // setAllUsers.add(Number(item2.media.user.id))

              newAccs2.push({
                user_id: item2.media.user.id,
                full_name: item2.media.user.full_name ? item2.media.user.full_name.replace(/\n/g, " ").replace(/[^0-9a-zA-Z, а-яА-Я]/gi, '') : '',
                is_private: item2.media.user.is_private ? 1 : 0,
                username: item2.media.user.username,
                cid: cid

              })

              timeRange.push(item2.media.taken_at)

              if (item2.media.likers) {

                if (item2.media.likers.length > 0) {

                  item2.media.likers.map(async (item3) => {

                    // setAllUsers.add(Number(item3.pk_id))

                    newAccs2.push({
                      user_id: item3.pk_id,
                      full_name: item3.full_name ? item3.full_name.replace(/\n/g, " ").replace(/[^0-9a-zA-Z, а-яА-Я]/gi, '') : '',
                      is_private: item3.is_private ? 1 : 0,
                      username: item3.username,
                      cid: cid

                    })

                  })

                }

              }

            })

          }

        })


        orTimeRange = _.orderBy(timeRange, item => item, ['desc'])
        orNewAccs2 = _.filter(newAccs2, function (o) { return o.user_id < 9953125287 })
        orNewAccs2 = _.uniqBy(orNewAccs2, 'user_id');
        // let fOA = filterObjects(orNewAccs);


        if (orNewAccs2.length > 0) {

          // saveToCSV(orNewAccs2, 'data3.csv')
          try {
            splitDataIntoCSVFiles(orNewAccs2)
          } catch (err) {
            console.error('error')
          }

          if (orTimeRange.length > 1) {
            time1 = dayjs.unix(orTimeRange[0])
            time2 = dayjs.unix(orTimeRange[orTimeRange.length - 1])
            delCal = Math.abs(Math.round(time2.diff(time1, 'seconds')));


            if (delCal > dl && dl != 0) {
              del = dl
            } else {
              del = delCal
            }

            nextTime = new Date(dayjs().add(del, 'second'))
            try {
              let v1 = await prisma.location.update({
                where: {
                  locationId: Number(locid)
                },
                data: {
                  delay: del,
                  next: nextTime,
                  last: new Date(Date.now())
                }
              });


            } catch (error) {
              logger(error)
            }


          } else {
            try {
              let xx = await prisma.location.update({
                where: {
                  locationId: Number(locid)
                },
                data: {
                  nul: 2,
                  last: new Date(Date.now())
                }
              });



            } catch (error) {
              logger(error)
            }
          }

        } else {
          try {
            let xx = await prisma.location.update({
              where: {
                locationId: Number(locid)
              },
              data: {
                nul: 2,
                last: new Date(Date.now())
              }
            });



          } catch (error) {
            logger(error)
          }

        }

      } else {
        // console.log(locid,'Here 2');
        try {
          let xx = await prisma.location.update({
            where: {
              locationId: Number(locid)
            },
            data: {
              nul: 2,
              last: new Date(Date.now())
            }
          });
          // console.log(xx,locid)


        } catch (error) {
          logger(error)
        }
      }


      console.log(`>> Done: ${nme}. Total top: ${orNewAccs.length}/${newAccs.length}. Total recent: ${orNewAccs2.length}/${newAccs2.length}. Delay: ${delCal}/${del} Next: ${dayjs(nextTime).format('HH:mm:ss YYYY-MM-DD')}. D: ${dailyCounter} H: ${hourlyCounter}`)
      hourlyCounter++
      dailyCounter++




    })
    .catch(async (err) => {
      hourlyErrCounter++
      dailyErrCounter++
      if (err.response) {

        
        console.log('Err: ', `https://www.instagram.com/api/v1/locations/web_info/?location_id=${locid}`)

        // logger(err.message)
        console.log(err.response.status);
        console.log(err.message)
        console.log('typpe @1')

        // if (err.response.status == 500) {
        //   try {
        //     await prisma.location.update({
        //         where: {
        //             locationId: Number(accounts[locAccountCounter].locationId)
        //         },
        //         data: {
        //             nul: 1
        //         }
        //     });
        // } catch (error) {
        //     throw error;
        //     logger(error)
        // }
        // }

      } else if (err.request) {
        // notifier.notify('Error. No response');
        // console.log(err.request)
        console.log('Error. No response' + dayjs().format('HH:mm:ss'));
        console.log('typpe @2')
      } else {
        // Something happened in setting up the request that triggered an err
        console.log('err', err.message);
        console.log('typpe @3')
        if (!isChange) {
          changeIP()
        }

        // console.log();
        // notifier.notify(err.message);
      }

    })

}

const getIP = async () => {

  axios.get(`https://api.myip.com/`, {
    httpsAgent: agent,
  })
    .then(async (res) => {
      console.log(res.data)
    })
    .catch(async (err) => {
      if (err.response) {
        // console.log(err.response.data);
        console.log(err.response.status);
        console.log(err.message)
      } else if (err.request) {
        // notifier.notify('Error. No response');
        // console.log(err.request)
        console.log('Error. No response' + dayjs().format('HH:mm:ss'));
      } else {
        // Something happened in setting up the request that triggered an err
        console.log('err', err.message);
        // notifier.notify(err.message);
      }

    })

}

const changeIP = async () => {

  console.log("Changing IP")

  isChange = true

  // axios.get(`http://176.9.113.112:11126/changeip/client/23108983551657110673`, {
  axios.get(process.env.CHANGE_IP)
    .then(async (res) => {

      if (res.status == 200) {
        // if (res.data.includes('completed')) {
        console.log("IP succesffully changed.")
        isChange = false
        return true

      } else {
        console.log(res.data)
        changeIP()
      }

    })
    .catch(async (err) => {
      console.log(err)
      setTimeout(() => isChange = false, 5000);
      console.log("Error while changing IP")
      if (err.response) {
        // console.log(err.response.data);
        console.log("IP " + err.response.status);
        console.log("IP " + err.message)
      } else if (err.request) {
        // notifier.notify('Error. No response');
        // console.log(err.request)
        console.log("IP " + 'Error. No response' + dayjs().format('HH:mm:ss'));
      } else {
        // Something happened in setting up the request that triggered an err
        console.log("IP ", 'err', err.message);
        // notifier.nfy(err.message);
      }
      return false
    })

}

function formatNumber(number) {
  const numberStr = number.toString();
  if (numberStr.includes('.')) {
    // Split the number into whole and fractional parts
    const [wholePart, fractionalPart] = numberStr.split('.');
    // Determine how many zeros are needed
    const zerosNeeded = 6 - fractionalPart.length;
    // Append the necessary number of zeros
    return wholePart + '.' + fractionalPart + '0'.repeat(zerosNeeded);
  } else {
    return numberStr + '.000000';
  }
}

function logger(message) {
  console.log(message)
  axios.post(logUrl, message, { headers: { 'Content-Type': 'text/plain' } })
    .then(response => {
      // console.log('Logged successfully:', response.data);
    })
    .catch(err => {
      console.error('Error during logging:', err.message);
    });
  
    
}

let hourlyCounter = 0;
let dailyCounter = 0;

let hourlyErrCounter = 0;
let dailyErrCounter = 0;

function resetHourlyCounter() {
  let message = 'Hourly: ' + hourlyCounter + ' / '+ hourlyErrCounter + '. Daily: ' + dailyCounter + ' / ' + dailyErrCounter + '. Delay: ' + intDelay;
  axios.post(logUrl, message, { headers: { 'Content-Type': 'text/plain' } })
    .then(response => {
      // console.log('Logged successfully:', response.data);
    })
    .catch(err => {
      console.error('Error during logging:', err.message);
    });

    
  hourlyCounter = 0;
  hourlyErrCounter = 0;
  console.log('Hourly counter has been reset to 0.');
}

function resetDailyCounter() {
  dailyCounter = 0;
  dailyErrCounter = 0;
  console.log('Daily counter has been reset to 0.');
}
// Schedule the resetHourlyCounter function to run at 00 minutes of every hour
const hourlyJob = schedule.scheduleJob('0 * * * *', resetHourlyCounter);

// Schedule the resetDailyCounter function to run at midnight every day
const dailyJob = schedule.scheduleJob('0 0 * * *', resetDailyCounter);

const ipChangeJob = schedule.scheduleJob('*/10 * * * *', changeIP);


// in case the script has to be restarted, line below will get index of file that we should start writing from
if (fs.readdirSync('./output').filter(value => value.includes('output_')).length > 0) {
  currentFileIndex = getCurrentFileIndex(maxFileSize)
}

Start()


