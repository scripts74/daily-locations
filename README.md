# Daily locations web scrapper

## Environment Variables

For all environment variables that are needed for operation, refer to **.env.sample** file

MAX_FILE_SIZE_MB = 1 **// this sets max size for csv file to 1 mb**

CHECK_FILE_SIZE_EVERY_X_MINS = 1 **// script will check every 1 min if maximum file size has been achieved**


## Output
Output will be written in sequence of output.csv files. Ex: output_1.csv, output_2.csv and so on

