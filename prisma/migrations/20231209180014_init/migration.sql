-- CreateTable
CREATE TABLE "user" (
    "id" BIGSERIAL NOT NULL,
    "user_id" BIGINT NOT NULL,
    "username" TEXT,
    "full_name" TEXT,
    "is_private" INTEGER,
    "times" INTEGER NOT NULL DEFAULT 1,
    "following" INTEGER,
    "followers" INTEGER,
    "biography" TEXT,
    "is_business" INTEGER,
    "category" TEXT,
    "is_verified" INTEGER,
    "posts" INTEGER,
    "cyrl" INTEGER,
    "is_processed" INTEGER NOT NULL DEFAULT 0,
    "locstring" TEXT DEFAULT '',
    "sx" INTEGER NOT NULL DEFAULT 0,
    "age" TEXT DEFAULT '',
    "amed" DOUBLE PRECISION,
    "amea" DOUBLE PRECISION,
    "male" INTEGER NOT NULL DEFAULT 0,
    "child" INTEGER NOT NULL DEFAULT 0,
    "img_processed" INTEGER NOT NULL DEFAULT 0,
    "videos" INTEGER NOT NULL DEFAULT 0,
    "minus" INTEGER NOT NULL DEFAULT 0,
    "deleted" INTEGER NOT NULL DEFAULT 0,
    "lastpost" TIMESTAMP(3),
    "a_like" INTEGER,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "is_prof" INTEGER,
    "cat_show" INTEGER,
    "bio_entity" INTEGER,
    "is_link" INTEGER,
    "reel_count" INTEGER NOT NULL DEFAULT 0,
    "pin_count" INTEGER NOT NULL DEFAULT 0,
    "sidecars" INTEGER NOT NULL DEFAULT 0,
    "is_hide" INTEGER,
    "a_view" INTEGER,
    "a_com" INTEGER,
    "a_post" INTEGER,
    "p_sx" INTEGER NOT NULL DEFAULT 0,
    "p_sx150" INTEGER NOT NULL DEFAULT 0,
    "is_fit" INTEGER NOT NULL DEFAULT 0,
    "is_s3" INTEGER NOT NULL DEFAULT 0,
    "cyrl_loc" TEXT,
    "cyrl_loc2" TEXT,
    "for_loc" TEXT,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "post" (
    "id" BIGSERIAL NOT NULL,
    "user_id" BIGINT,
    "url" TEXT,
    "type" INTEGER NOT NULL DEFAULT 0,

    CONSTRAINT "post_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "location" (
    "id" SERIAL NOT NULL,
    "location_id" INTEGER NOT NULL,
    "city" VARCHAR(100),
    "country_id" VARCHAR(5),
    "is_processed" INTEGER NOT NULL DEFAULT 0,
    "times" INTEGER NOT NULL DEFAULT 1,
    "name" TEXT,
    "slug" VARCHAR(255),
    "has_public_page" INTEGER,
    "lat" DOUBLE PRECISION,
    "lon" DOUBLE PRECISION,
    "state" TEXT,
    "nul" INTEGER DEFAULT 0,
    "delay" INTEGER DEFAULT 0,
    "next" TIMESTAMP(3),
    "last" TIMESTAMP(3),

    CONSTRAINT "location_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_user_id_key" ON "user"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "location_location_id_key" ON "location"("location_id");

-- AddForeignKey
ALTER TABLE "post" ADD CONSTRAINT "post_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user"("user_id") ON DELETE SET NULL ON UPDATE CASCADE;
