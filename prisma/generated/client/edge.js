
Object.defineProperty(exports, "__esModule", { value: true });

const {
  PrismaClientKnownRequestError,
  PrismaClientUnknownRequestError,
  PrismaClientRustPanicError,
  PrismaClientInitializationError,
  PrismaClientValidationError,
  NotFoundError,
  getPrismaClient,
  sqltag,
  empty,
  join,
  raw,
  Decimal,
  Debug,
  objectEnumValues,
  makeStrictEnum,
  Extensions,
  warnOnce,
  defineDmmfProperty,
  Public,
} = require('./runtime/edge')


const Prisma = {}

exports.Prisma = Prisma
exports.$Enums = {}

/**
 * Prisma Client JS version: 5.3.1
 * Query Engine version: 61e140623197a131c2a6189271ffee05a7aa9a59
 */
Prisma.prismaVersion = {
  client: "5.3.1",
  engine: "61e140623197a131c2a6189271ffee05a7aa9a59"
}

Prisma.PrismaClientKnownRequestError = PrismaClientKnownRequestError;
Prisma.PrismaClientUnknownRequestError = PrismaClientUnknownRequestError
Prisma.PrismaClientRustPanicError = PrismaClientRustPanicError
Prisma.PrismaClientInitializationError = PrismaClientInitializationError
Prisma.PrismaClientValidationError = PrismaClientValidationError
Prisma.NotFoundError = NotFoundError
Prisma.Decimal = Decimal

/**
 * Re-export of sql-template-tag
 */
Prisma.sql = sqltag
Prisma.empty = empty
Prisma.join = join
Prisma.raw = raw
Prisma.validator = Public.validator

/**
* Extensions
*/
Prisma.getExtensionContext = Extensions.getExtensionContext
Prisma.defineExtension = Extensions.defineExtension

/**
 * Shorthand utilities for JSON filtering
 */
Prisma.DbNull = objectEnumValues.instances.DbNull
Prisma.JsonNull = objectEnumValues.instances.JsonNull
Prisma.AnyNull = objectEnumValues.instances.AnyNull

Prisma.NullTypes = {
  DbNull: objectEnumValues.classes.DbNull,
  JsonNull: objectEnumValues.classes.JsonNull,
  AnyNull: objectEnumValues.classes.AnyNull
}



/**
 * Enums
 */
exports.Prisma.TransactionIsolationLevel = makeStrictEnum({
  ReadUncommitted: 'ReadUncommitted',
  ReadCommitted: 'ReadCommitted',
  RepeatableRead: 'RepeatableRead',
  Serializable: 'Serializable'
});

exports.Prisma.UserScalarFieldEnum = {
  id: 'id',
  userId: 'userId',
  username: 'username',
  fullName: 'fullName',
  isPrivate: 'isPrivate',
  times: 'times',
  following: 'following',
  followers: 'followers',
  biography: 'biography',
  isBusiness: 'isBusiness',
  category: 'category',
  isVerified: 'isVerified',
  postCount: 'postCount',
  cyrl: 'cyrl',
  isProcessed: 'isProcessed',
  locString: 'locString',
  sx: 'sx',
  age: 'age',
  amed: 'amed',
  amea: 'amea',
  male: 'male',
  child: 'child',
  imgProcessed: 'imgProcessed',
  videos: 'videos',
  minus: 'minus',
  deleted: 'deleted',
  lastpost: 'lastpost',
  aLike: 'aLike',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  isProf: 'isProf',
  catShow: 'catShow',
  bioEntity: 'bioEntity',
  isLink: 'isLink',
  reelCount: 'reelCount',
  pinCount: 'pinCount',
  sidecars: 'sidecars',
  isHide: 'isHide',
  aView: 'aView',
  aCom: 'aCom',
  aPost: 'aPost',
  pSx: 'pSx',
  pSx150: 'pSx150',
  isFit: 'isFit',
  isS3: 'isS3',
  cyrlLoc: 'cyrlLoc',
  cyrlLoc2: 'cyrlLoc2',
  forLoc: 'forLoc'
};

exports.Prisma.PostScalarFieldEnum = {
  id: 'id',
  userId: 'userId',
  url: 'url',
  type: 'type'
};

exports.Prisma.LocationScalarFieldEnum = {
  id: 'id',
  locationId: 'locationId',
  city: 'city',
  countryId: 'countryId',
  isProcessed: 'isProcessed',
  times: 'times',
  name: 'name',
  slug: 'slug',
  hasPublicPage: 'hasPublicPage',
  lat: 'lat',
  lon: 'lon',
  state: 'state',
  nul: 'nul',
  delay: 'delay',
  next: 'next',
  last: 'last'
};

exports.Prisma.SortOrder = {
  asc: 'asc',
  desc: 'desc'
};

exports.Prisma.QueryMode = {
  default: 'default',
  insensitive: 'insensitive'
};

exports.Prisma.NullsOrder = {
  first: 'first',
  last: 'last'
};


exports.Prisma.ModelName = {
  User: 'User',
  Post: 'Post',
  Location: 'Location'
};
/**
 * Create the Client
 */
const config = {
  "generator": {
    "name": "client",
    "provider": {
      "fromEnvVar": null,
      "value": "prisma-client-js"
    },
    "output": {
      "value": "/home/gaurav/Desktop/daily-locations/prisma/generated/client",
      "fromEnvVar": null
    },
    "config": {
      "engineType": "library"
    },
    "binaryTargets": [
      {
        "fromEnvVar": null,
        "value": "debian-openssl-3.0.x",
        "native": true
      },
      {
        "fromEnvVar": null,
        "value": "debian-openssl-1.1.x"
      },
      {
        "fromEnvVar": null,
        "value": "debian-openssl-3.0.x"
      },
      {
        "fromEnvVar": null,
        "value": "linux-musl"
      }
    ],
    "previewFeatures": [],
    "isCustomOutput": true
  },
  "relativeEnvPaths": {
    "rootEnvPath": "../../../.env",
    "schemaEnvPath": "../../../.env"
  },
  "relativePath": "../..",
  "clientVersion": "5.3.1",
  "engineVersion": "61e140623197a131c2a6189271ffee05a7aa9a59",
  "datasourceNames": [
    "db"
  ],
  "activeProvider": "postgresql",
  "inlineDatasources": {
    "db": {
      "url": {
        "fromEnvVar": "DATABASE_URL",
        "value": null
      }
    }
  },
  "inlineSchema": "Z2VuZXJhdG9yIGNsaWVudCB7CiAgcHJvdmlkZXIgICAgICAgID0gInByaXNtYS1jbGllbnQtanMiCiAgb3V0cHV0ICAgICAgICAgID0gIi4vZ2VuZXJhdGVkL2NsaWVudCIKICBiaW5hcnlUYXJnZXRzICAgPSBbIm5hdGl2ZSIsImRlYmlhbi1vcGVuc3NsLTEuMS54IiwiZGViaWFuLW9wZW5zc2wtMy4wLngiLCJsaW51eC1tdXNsIl0KfQoKZGF0YXNvdXJjZSBkYiB7CiAgcHJvdmlkZXIgPSAicG9zdGdyZXNxbCIKICB1cmwgICAgICA9IGVudigiREFUQUJBU0VfVVJMIikgLy8gUmVhZCBmcm9tIC5lbnYgZmlsZQp9Cgptb2RlbCBVc2VyIHsKICBpZCAgICAgICAgICAgIEJpZ0ludCAgIEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpIEBtYXAoImlkIikKICB1c2VySWQgICAgICAgIEJpZ0ludCAgIEB1bmlxdWUgQG1hcCgidXNlcl9pZCIpCiAgdXNlcm5hbWUgICAgICBTdHJpbmc/ICBAbWFwKCJ1c2VybmFtZSIpCiAgZnVsbE5hbWUgICAgICBTdHJpbmc/ICBAbWFwKCJmdWxsX25hbWUiKQogIGlzUHJpdmF0ZSAgICAgSW50PyAgICAgQG1hcCgiaXNfcHJpdmF0ZSIpCiAgdGltZXMgICAgICAgICBJbnQgICAgICBAZGVmYXVsdCgxKSBAbWFwKCJ0aW1lcyIpCiAgZm9sbG93aW5nICAgICBJbnQ/ICAgICBAbWFwKCJmb2xsb3dpbmciKQogIGZvbGxvd2VycyAgICAgSW50PyAgICAgQG1hcCgiZm9sbG93ZXJzIikKICBiaW9ncmFwaHkgICAgIFN0cmluZz8gIEBtYXAoImJpb2dyYXBoeSIpCiAgaXNCdXNpbmVzcyAgICBJbnQ/ICAgICBAbWFwKCJpc19idXNpbmVzcyIpCiAgY2F0ZWdvcnkgICAgICBTdHJpbmc/ICBAbWFwKCJjYXRlZ29yeSIpCiAgaXNWZXJpZmllZCAgICBJbnQ/ICAgICBAbWFwKCJpc192ZXJpZmllZCIpCiAgcG9zdENvdW50ICAgICBJbnQ/ICAgICBAbWFwKCJwb3N0cyIpCiAgY3lybCAgICAgICAgICBJbnQ/ICAgICBAbWFwKCJjeXJsIikKICBpc1Byb2Nlc3NlZCAgIEludCAgICAgIEBkZWZhdWx0KDApIEBtYXAoImlzX3Byb2Nlc3NlZCIpCiAgbG9jU3RyaW5nICAgICBTdHJpbmc/ICAgQGRlZmF1bHQoIiIpIEBtYXAoImxvY3N0cmluZyIpCiAgc3ggICAgICAgICAgICBJbnQgICAgICBAZGVmYXVsdCgwKSBAbWFwKCJzeCIpCiAgYWdlICAgICAgICAgICBTdHJpbmc/ICAgQGRlZmF1bHQoIiIpIEBtYXAoImFnZSIpCiAgYW1lZCAgICAgICAgICBGbG9hdD8gICBAbWFwKCJhbWVkIikKICBhbWVhICAgICAgICAgIEZsb2F0PyAgIEBtYXAoImFtZWEiKQogIG1hbGUgICAgICAgICAgSW50ICAgICAgQGRlZmF1bHQoMCkgQG1hcCgibWFsZSIpCiAgY2hpbGQgICAgICAgICBJbnQgICAgICBAZGVmYXVsdCgwKSBAbWFwKCJjaGlsZCIpCiAgaW1nUHJvY2Vzc2VkICBJbnQgICAgICBAZGVmYXVsdCgwKSBAbWFwKCJpbWdfcHJvY2Vzc2VkIikKICB2aWRlb3MgICAgICAgIEludCAgICAgIEBkZWZhdWx0KDApIEBtYXAoInZpZGVvcyIpCiAgbWludXMgICAgICAgICBJbnQgICAgICBAZGVmYXVsdCgwKSBAbWFwKCJtaW51cyIpCiAgZGVsZXRlZCAgICAgICBJbnQgICAgICBAZGVmYXVsdCgwKSBAbWFwKCJkZWxldGVkIikKICBsYXN0cG9zdCAgICAgIERhdGVUaW1lPyBAbWFwKCJsYXN0cG9zdCIpCiAgYUxpa2UgICAgICAgICBJbnQ/ICAgICBAbWFwKCJhX2xpa2UiKQogIGNyZWF0ZWRBdCAgICAgRGF0ZVRpbWUgQGRlZmF1bHQobm93KCkpIEBtYXAoImNyZWF0ZWRfYXQiKQogIHVwZGF0ZWRBdCAgICAgRGF0ZVRpbWUgQGRlZmF1bHQobm93KCkpIEBtYXAoInVwZGF0ZWRfYXQiKQogIAogIGlzUHJvZiAgICAgICBJbnQ/ICAgIEBtYXAoImlzX3Byb2YiKQogIGNhdFNob3cgICAgICBJbnQ/ICAgIEBtYXAoImNhdF9zaG93IikKICBiaW9FbnRpdHkgICAgSW50PyAgICAgQG1hcCgiYmlvX2VudGl0eSIpCiAgaXNMaW5rICAgICAgIEludD8gICAgQG1hcCgiaXNfbGluayIpCiAgcmVlbENvdW50ICAgIEludCAgICAgICAgIEBkZWZhdWx0KDApIEBtYXAoInJlZWxfY291bnQiKQogIHBpbkNvdW50ICAgICBJbnQgICAgICAgICBAZGVmYXVsdCgwKSBAbWFwKCJwaW5fY291bnQiKQogIHNpZGVjYXJzICAgICBJbnQgICAgICAgICBAZGVmYXVsdCgwKSBAbWFwKCJzaWRlY2FycyIpCiAgaXNIaWRlICAgICAgIEludD8gICAgQG1hcCgiaXNfaGlkZSIpCiAgYVZpZXcgICAgICAgIEludD8gICAgICAgIEBtYXAoImFfdmlldyIpCiAgYUNvbSAgICAgICAgIEludD8gICAgICAgIEBtYXAoImFfY29tIikKICBhUG9zdCAgICAgICAgSW50PyAgICAgICAgQG1hcCgiYV9wb3N0IikKICBwU3ggICAgICAgICAgSW50ICAgICBAZGVmYXVsdCgwKSBAbWFwKCJwX3N4IikKICBwU3gxNTAgICAgICAgSW50ICAgICBAZGVmYXVsdCgwKSBAbWFwKCJwX3N4MTUwIikKICBpc0ZpdCAgICAgICBJbnQgICAgIEBkZWZhdWx0KDApIEBtYXAoImlzX2ZpdCIpCiAgaXNTMyAgICAgICBJbnQgICAgIEBkZWZhdWx0KDApIEBtYXAoImlzX3MzIikKICBjeXJsTG9jIFN0cmluZz8gQG1hcCgiY3lybF9sb2MiKQogIGN5cmxMb2MyIFN0cmluZz8gQG1hcCgiY3lybF9sb2MyIikKICBmb3JMb2MgU3RyaW5nPyBAbWFwKCJmb3JfbG9jIikKCiAgcG9zdHMgICAgICAgICBQb3N0W10KICAKICBAQG1hcCgidXNlciIpCn0KCm1vZGVsIFBvc3QgewogIGlkICAgICAgQmlnSW50IEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpCiAgdXNlcklkICBCaWdJbnQ/IEBtYXAoInVzZXJfaWQiKQogIHVybCAgICAgU3RyaW5nPyAgQG1hcCgidXJsIikKICB0eXBlICAgSW50ICAgICAgQGRlZmF1bHQoMCkgQG1hcCgidHlwZSIpCiAgdXNlciAgICBVc2VyPyAgIEByZWxhdGlvbihmaWVsZHM6IFt1c2VySWRdLCByZWZlcmVuY2VzOiBbdXNlcklkXSkKICAKICBAQG1hcCgicG9zdCIpCn0KCm1vZGVsIExvY2F0aW9uIHsKICBpZCAgICAgICAgICAgIEludCAgICAgIEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpIEBtYXAoImlkIikKICBsb2NhdGlvbklkICAgIEludCAgICAgIEB1bmlxdWUgQG1hcCgibG9jYXRpb25faWQiKQogIGNpdHkgICAgICAgICAgU3RyaW5nPyAgQG1hcCgiY2l0eSIpIEBkYi5WYXJDaGFyKDEwMCkKICBjb3VudHJ5SWQgICAgIFN0cmluZz8gIEBtYXAoImNvdW50cnlfaWQiKSBAZGIuVmFyQ2hhcig1KQogIGlzUHJvY2Vzc2VkICAgSW50ICAgICAgQGRlZmF1bHQoMCkgQG1hcCgiaXNfcHJvY2Vzc2VkIikgLy8gT3IgQm9vbGVhbiBAZGVmYXVsdChmYWxzZSkgaWYgaXQgbWFrZXMgc2Vuc2UgaW4geW91ciBjYXNlCiAgdGltZXMgICAgICAgICBJbnQgICAgICBAZGVmYXVsdCgxKSBAbWFwKCJ0aW1lcyIpCiAgbmFtZSAgICAgICAgICBTdHJpbmc/ICBAbWFwKCJuYW1lIikgQGRiLlRleHQKICBzbHVnICAgICAgICAgIFN0cmluZz8gIEBtYXAoInNsdWciKSBAZGIuVmFyQ2hhcigyNTUpCiAgaGFzUHVibGljUGFnZSBJbnQ/ICAgICBAbWFwKCJoYXNfcHVibGljX3BhZ2UiKSAvLyBPciBCb29sZWFuPyBpZiBpdCBtYWtlcyBzZW5zZSBpbiB5b3VyIGNhc2UKICBsYXQgICAgICAgICAgICBGbG9hdD8gICBAbWFwKCJsYXQiKQogIGxvbiAgICAgICAgICAgIEZsb2F0PyAgIEBtYXAoImxvbiIpCiAgc3RhdGUgICAgICAgICAgU3RyaW5nPyAgQG1hcCgic3RhdGUiKQogIG51bCAgICAgICAgICBJbnQ/ICAgICAgQGRlZmF1bHQoMCkgQG1hcCgibnVsIikKICBkZWxheSBJbnQ/IEBkZWZhdWx0KDApIEBtYXAoImRlbGF5IikKICBuZXh0IERhdGVUaW1lPyBAbWFwKCJuZXh0IikKICBsYXN0IERhdGVUaW1lPyBAbWFwKCJsYXN0IikKCiAgQEBtYXAoImxvY2F0aW9uIikKfQoKCg==",
  "inlineSchemaHash": "4a23cc9a37f725324cb231e93f24a0cd01486cd9f63436dae1418df5b0e9b32a"
}
config.dirname = '/'

config.runtimeDataModel = JSON.parse("{\"models\":{\"User\":{\"dbName\":\"user\",\"fields\":[{\"name\":\"id\",\"dbName\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"BigInt\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"userId\",\"dbName\":\"user_id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":true,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"BigInt\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"username\",\"dbName\":\"username\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"fullName\",\"dbName\":\"full_name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isPrivate\",\"dbName\":\"is_private\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"times\",\"dbName\":\"times\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":1,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"following\",\"dbName\":\"following\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"followers\",\"dbName\":\"followers\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"biography\",\"dbName\":\"biography\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isBusiness\",\"dbName\":\"is_business\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"category\",\"dbName\":\"category\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isVerified\",\"dbName\":\"is_verified\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"postCount\",\"dbName\":\"posts\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"cyrl\",\"dbName\":\"cyrl\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isProcessed\",\"dbName\":\"is_processed\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"locString\",\"dbName\":\"locstring\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"sx\",\"dbName\":\"sx\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"age\",\"dbName\":\"age\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"amed\",\"dbName\":\"amed\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Float\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"amea\",\"dbName\":\"amea\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Float\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"male\",\"dbName\":\"male\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"child\",\"dbName\":\"child\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"imgProcessed\",\"dbName\":\"img_processed\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"videos\",\"dbName\":\"videos\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"minus\",\"dbName\":\"minus\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"deleted\",\"dbName\":\"deleted\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"lastpost\",\"dbName\":\"lastpost\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DateTime\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"aLike\",\"dbName\":\"a_like\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"dbName\":\"created_at\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"updatedAt\",\"dbName\":\"updated_at\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isProf\",\"dbName\":\"is_prof\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"catShow\",\"dbName\":\"cat_show\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"bioEntity\",\"dbName\":\"bio_entity\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isLink\",\"dbName\":\"is_link\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"reelCount\",\"dbName\":\"reel_count\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"pinCount\",\"dbName\":\"pin_count\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"sidecars\",\"dbName\":\"sidecars\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isHide\",\"dbName\":\"is_hide\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"aView\",\"dbName\":\"a_view\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"aCom\",\"dbName\":\"a_com\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"aPost\",\"dbName\":\"a_post\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"pSx\",\"dbName\":\"p_sx\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"pSx150\",\"dbName\":\"p_sx150\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isFit\",\"dbName\":\"is_fit\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isS3\",\"dbName\":\"is_s3\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"cyrlLoc\",\"dbName\":\"cyrl_loc\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"cyrlLoc2\",\"dbName\":\"cyrl_loc2\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"forLoc\",\"dbName\":\"for_loc\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"posts\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Post\",\"relationName\":\"PostToUser\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Post\":{\"dbName\":\"post\",\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"BigInt\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"userId\",\"dbName\":\"user_id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"BigInt\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"url\",\"dbName\":\"url\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"type\",\"dbName\":\"type\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"user\",\"kind\":\"object\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"PostToUser\",\"relationFromFields\":[\"userId\"],\"relationToFields\":[\"userId\"],\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Location\":{\"dbName\":\"location\",\"fields\":[{\"name\":\"id\",\"dbName\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"locationId\",\"dbName\":\"location_id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":true,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"city\",\"dbName\":\"city\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"countryId\",\"dbName\":\"country_id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isProcessed\",\"dbName\":\"is_processed\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"times\",\"dbName\":\"times\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":1,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"dbName\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"slug\",\"dbName\":\"slug\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"hasPublicPage\",\"dbName\":\"has_public_page\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"lat\",\"dbName\":\"lat\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Float\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"lon\",\"dbName\":\"lon\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Float\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"state\",\"dbName\":\"state\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"nul\",\"dbName\":\"nul\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"delay\",\"dbName\":\"delay\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"next\",\"dbName\":\"next\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DateTime\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"last\",\"dbName\":\"last\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DateTime\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false}},\"enums\":{},\"types\":{}}")
defineDmmfProperty(exports.Prisma, config.runtimeDataModel)


config.injectableEdgeEnv = () => ({
  parsed: {
    DATABASE_URL: typeof globalThis !== 'undefined' && globalThis['DATABASE_URL'] || typeof process !== 'undefined' && process.env && process.env.DATABASE_URL || undefined
  }
})

if (typeof globalThis !== 'undefined' && globalThis['DEBUG'] || typeof process !== 'undefined' && process.env && process.env.DEBUG || undefined) {
  Debug.enable(typeof globalThis !== 'undefined' && globalThis['DEBUG'] || typeof process !== 'undefined' && process.env && process.env.DEBUG || undefined)
}

const PrismaClient = getPrismaClient(config)
exports.PrismaClient = PrismaClient
Object.assign(exports, Prisma)

